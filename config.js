const config = {
    "backupFolder": "",
    "mongo": {
        "host": "",
        "db": "",
        "username": "",
        "password": ""
    },
    "googleDrive": {
        "credentialFile": "./creds.json",
        "rootFolder": "",

    }
};

module.exports = config;