import {
    createDumpCommand,
    executeCommand
} from "ale-mongoutils";

const config = require("./config.js");
const driveCredentials = require(path.resolve(config.googleDrive.credentialFile));

let dump = function() {
    return new Promise(function(res, rej) {
        let command = createDumpCommand({
            database: config.mongo.db,
            host: config.mongo.host, // e.g. "mongodb://localhost:27017"
            dist: config.backupFolder
        });

        executeCommand(command, function(out) {
            res("OUT", out);
        });
    });
};

function theDate() {
    let date = new Date();
    date = date.getUTCFullYear() + "-" + (
        "00" + (
            date.getUTCMonth() + 1)).slice(-2) + "-" + (
        "00" + date.getUTCDate()).slice(-2) + " " + (
        "00" + date.getUTCHours()).slice(-2) + ":" + (
        "00" + date.getUTCMinutes()).slice(-2) + ":" + (
        "00" + date.getUTCSeconds()).slice(-2);
    return date;
}




// Let's wrap everything in an async function to use await sugar
async function ExampleOperations() {


    const googleDriveInstance = new NodeGoogleDrive({
        ROOT_FOLDER: config.googleDrive.rootFolder
    });

    let gdrive = await googleDriveInstance.useServiceAccountAuth(driveCredentials);

    // List Folders under the root folder
    let folderResponse = await googleDriveInstance.listFolders(config.googleDrive.rootFolder, null, false);

    console.log({
        folders: folderResponse.folders
    });

    // Create a folder under your root folder
    let newFolder = {
            name: "folder_example" + Date.now()
        },
        createFolderResponse = await googleDriveInstance.createFolder(YOUR_ROOT_FOLDER, newFolder.name);

    newFolder.id = createFolderResponse.id;

    debug(`Created folder ${newFolder.name} with id ${newFolder.id}`);

    // List files under your root folder.
    let listFilesResponse = await googleDriveInstance.listFiles(YOUR_ROOT_FOLDER, null, false);

    for (let file of listFilesResponse.files) {
        debug({
            file
        });
    }
}

ExampleOperations();
